<html lang="en">
<!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->

<head>
	<title>System info</title>
	<meta charset="utf-8">
	<meta http-equiv="refresh" content="15">
	<link rel="shortcut icon" href="favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/css/uikit.min.css" />
	<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit-icons.min.js"></script>
	<script src="justgage.js"></script>
	<script src="raphael.min.js"></script>
</head>

<body>
	<?php
	$temp = shell_exec('cat /sys/class/thermal/thermal_zone*/temp');
	$temp = round($temp / 1000, 1);
	$cpuusage = 100 - shell_exec("vmstat | tail -1 | awk '{print $15}'");
	$mem = shell_exec("free | grep Mem | awk '{print $3/$2 * 100.0}'");
	$mem = round($mem, 1);
	?>
	<div class="uk-container uk-container-small uk-margin-top">
		<h1 class="uk-card-title uk-heading-line uk-text-center"><span>System info</span></h1>
		<div class="uk-child-width-expand@s uk-text-center" uk-grid>
			<div>
				<div class="uk-card uk-card-default uk-card-body">
					<?php
					if (isset($temp) && is_numeric($temp)) { ?>
						<div id="tempgauge"></div>
						<script>
							var t = new JustGage({
								id: "tempgauge",
								value: <?php echo $temp; ?>,
								min: 0,
								max: 100,
								height: 200,
								width: 200,
								title: "Temperature",
								label: "°C"
							});
						</script>
					<?php } ?>
				</div>
			</div>
			<div>
				<div class="uk-card uk-card-default uk-card-body">
					<?php if (isset($cpuusage) && is_numeric($cpuusage)) { ?>
						<div id="cpugauge"></div>
						<script>
							var u = new JustGage({
								id: "cpugauge",
								value: <?php echo $cpuusage; ?>,
								min: 0,
								max: 100,
								height: 200,
								width: 200,
								title: "CPU Load",
								label: "%"
							});
						</script>

					<?php } ?>
				</div>
			</div>
			<div>
				<div class="uk-card uk-card-default uk-card-body"><?php if (isset($mem) && is_numeric($mem)) { ?>
						<div id="memgauge"></div>
						<script>
							var u = new JustGage({
								id: "memgauge",
								value: <?php echo $mem; ?>,
								min: 0,
								max: 100,
								height: 200,
								width: 200,
								title: "Memory",
								label: "%"
							});
						</script>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="uk-container uk-margin-top">
			<div class="uk-card uk-card-default uk-card-body">
				<h3 class="uk-card-title uk-heading-line uk-text-center"><span>Devices</span></h3>
				<?php
				echo '<pre>';
				passthru("lsblk");
				echo '</pre>';
				?>
			</div>
		</div>
		<div class="uk-card uk-card-default uk-card-body">
			<h3 class="uk-card-title uk-heading-line uk-text-center"><span>Disk space</span></h3>
			<?php
			echo '<pre>';
			passthru("df -H");
			echo '</pre>';
			?>
		</div>
	</div>
</body>
</html>